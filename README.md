### Python version

3.9.0

### Package Requirements

scrapy

### Question 1-4
Use the following command to start the unittest

`python -m unittest`

### Question 5-6
SQL Version:

`Microsoft SQL Server 2019 (RTM) - 15.0.2000.5 (X64)   Sep 24 2019 13:48:23   Copyright (C) 2019 Microsoft Corporation  Developer Edition (64-bit) on Windows 10 Pro 10.0 <X64> (Build 19041: ) `

### Question 7
Skipped

### Question 8
The main logic is in the python file `LinkChecker.py` which is in the `spiders` folder.
The `start_urls` variable is the list of urls.
If you want to use the local html files, put the files in the `htmls` folder and use the absolute path.
Start the link checker with:

```
python -m venv venv
source venv/Scripts/activate
pip install -r requirements.txt

cd q8
scrapy crawl
```
After the crawling done, there will be a new file `links.txt` created in the `q8` folder.
All the available links are listing in this file.
