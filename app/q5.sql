-- Question 5_A
SELECT DISTINCT s.name
FROM orders o,
  customer c,
  salesperson s
WHERE o.customerid = c.customerid
      AND s.salespersonid = o.salespersonid
      AND c.name = 'George';

-- Question 5_B
SELECT DISTINCT salesperson.name
FROM salesperson
WHERE salesperson.salespersonid NOT IN
      (
        SELECT DISTINCT s.salespersonid
        FROM orders o,
          customer c,
          salesperson s
        WHERE o.customerid = c.customerid
              AND s.salespersonid = o.salespersonid
              AND c.name = 'George'
      );

-- Question 5_C
SELECT
  s.name,
  ordercount.ordernumber
FROM salesperson s,
  (SELECT
     s.salespersonid,
     count(s.name) ordernumber
   FROM salesperson s,
     orders o
   WHERE s.salespersonid = o.salespersonid
   GROUP BY s.salespersonid) ordercount
WHERE s.salespersonid = ordercount.salespersonid
      AND ordercount.ordernumber > 1;

