-- Question 6_A
SELECT *
FROM salesperson
ORDER BY salary DESC OFFSET 2 ROWS
FETCH NEXT 1 ROW ONLY;

-- Question 6_B
CREATE TABLE bigorders
(
  customerid      INT,
  totalordervalue INT
);
INSERT INTO bigorders (customerid, totalordervalue)
  SELECT *
  FROM (
         SELECT
           customerid,
           sum(numberofunits * costofunit) totalAmount
         FROM orders
         GROUP BY customerid) AS A
  WHERE totalAmount > 1000;

-- Question 6_C
SELECT
  DATEPART(YEAR, orderdate)  year,
  DATEPART(MONTH, orderdate) month,
  sum(numberofunits * costofunit)
FROM orders
GROUP BY DATEPART(YEAR, orderdate), DATEPART(MONTH, orderdate)
ORDER BY year DESC, month DESC;