class Solution:
    @staticmethod
    def isNullOrEmpty(val):
        if val is None:
            return True
        elif val == "":
            return True
        else:
            return False
