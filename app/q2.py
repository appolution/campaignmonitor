class Solution:
    @staticmethod
    def getPositiveDivisors(val):
        results = []
        isOdd = val % 2 == 1
        for i in range(1, val + 1):
            if isOdd and i % 2 == 0:
                # all divisors of an odd number are odd.
                continue
            if val % i == 0:
                results.append(i)
        return results
