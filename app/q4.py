class InvalidTriangleException(Exception):
    pass


class Solution:
    @staticmethod
    def getMostCommon(values):
        valueMap = {}
        for i in values:
            if i in valueMap:
                valueMap[i] += 1
            else:
                valueMap[i] = 1
        maxCount = 0
        result = []
        for key in valueMap:
            if valueMap[key] > maxCount:
                result = [key]
                maxCount = valueMap[key]
            elif valueMap[key] == maxCount:
                result.append(key)
        return result
