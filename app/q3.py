import math


class InvalidTriangleException(Exception):
    pass


class Solution:
    @staticmethod
    def getArea(a, b, c):
        if a < 0 or b < 0 or c < 0:
            raise InvalidTriangleException(a, b, c)
        if a + b < c or b + c < a or a + c < b:
            raise InvalidTriangleException(a, b, c)
        p = (a + b + c) / 2
        s = math.sqrt(p * (p - a) * (p - b) * (p - c))
        return s
