import unittest

from app.q3 import Solution, InvalidTriangleException


class Test(unittest.TestCase):
    def test1(self):
        """3 4 5"""
        actual = Solution.getArea(3, 4, 5)
        expected = 6
        self.assertEqual(expected, actual)

    def test2(self):
        """4 5 6"""
        actual = Solution.getArea(4, 5, 6)
        expected = 9.922
        self.assertAlmostEqual(expected, actual, 3)

    def test3(self):
        """11 15 6"""
        actual = Solution.getArea(11, 15, 6)
        expected = 28.284
        self.assertAlmostEqual(expected, actual, 3)

    def test4(self):
        """-3 4 5"""
        self.assertRaises(InvalidTriangleException, Solution.getArea, -3, 4, 5)

    def test5(self):
        """3 4 9"""
        self.assertRaises(InvalidTriangleException, Solution.getArea, 3, 4, 9)
