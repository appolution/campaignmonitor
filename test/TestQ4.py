import unittest

from app.q4 import Solution, InvalidTriangleException


class Test(unittest.TestCase):
    def test1(self):
        """[5, 4, 3, 2, 4, 5, 1, 6, 1, 2, 5, 4]"""
        actual = Solution.getMostCommon([5, 4, 3, 2, 4, 5, 1, 6, 1, 2, 5, 4])
        expected = [4, 5]
        self.assertEqual(sorted(expected), sorted(actual))

    def test2(self):
        """[1, 2, 3, 4, 5, 1, 6, 7]"""
        actual = Solution.getMostCommon([1, 2, 3, 4, 5, 1, 6, 7])
        expected = [1]
        self.assertEqual(sorted(expected), sorted(actual))

    def test3(self):
        """[1, 2, 3, 4, 5, 6, 7]"""
        actual = Solution.getMostCommon([1, 2, 3, 4, 5, 6, 7])
        expected = [1, 2, 3, 4, 5, 6, 7]
        self.assertEqual(sorted(expected), sorted(actual))
