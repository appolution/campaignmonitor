import unittest

from app.q2 import Solution


class Test(unittest.TestCase):
    def test1(self):
        """42"""
        actual = Solution.getPositiveDivisors(42)
        expected = [1, 2, 3, 6, 7, 14, 21, 42]
        self.assertEqual(expected, actual)

    def test2(self):
        """60"""
        actual = Solution.getPositiveDivisors(60)
        expected = [1, 2, 3, 4, 5, 6, 10, 12, 15, 20, 30, 60]
        self.assertEqual(expected, actual)

    def test3(self):
        """33"""
        actual = Solution.getPositiveDivisors(33)
        expected = [1, 3, 11, 33]
        self.assertEqual(expected, actual)

    def test4(self):
        """36"""
        actual = Solution.getPositiveDivisors(36)
        expected = [1, 2, 3, 4, 6, 9, 12, 18, 36]
        self.assertEqual(expected, actual)
