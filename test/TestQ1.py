import unittest

from app.q1 import Solution


class Test(unittest.TestCase):
    def test1(self):
        """Input is null"""
        actual = Solution.isNullOrEmpty(None)
        expected = True
        self.assertEqual(expected, actual)

    def test2(self):
        """Input is \"a\" """
        actual = Solution.isNullOrEmpty("a")
        expected = False
        self.assertEqual(expected, actual)

    def test3(self):
        """Input is Empty"""
        actual = Solution.isNullOrEmpty("")
        expected = True
        self.assertEqual(expected, actual)

    def test4(self):
        """Input is \"null\""""
        actual = Solution.isNullOrEmpty("null")
        expected = False
        self.assertEqual(expected, actual)

    def test5(self):
        """Input is \"None\""""
        actual = Solution.isNullOrEmpty("None")
        expected = False
        self.assertEqual(expected, actual)

if __name__ == '__main__':
    unittest.main()