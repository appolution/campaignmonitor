import scrapy
import urllib
from scrapy import cmdline
from urllib.error import URLError
from urllib.request import Request

import sys

urlMap = {}


class LinkcheckerSpider(scrapy.Spider):
    name = 'LinkChecker'
    allowed_domains = []
    start_urls = [
        'file:E:/workspace/python/CampaignMonitor/q8/q8/htmls/test1.html',
        'https://www.markdownguide.org/cheat-sheet/',
    ]
    totalLinks = len(start_urls)

    def parse(self, response):
        links = response.xpath('.//a/@href').extract()
        file = open('links.txt', 'a')
        for index, link in enumerate(links):
            if link == "#":
                continue
            if not link.startswith('/'):
                availableLink = link
            else:
                availableLink = response.url + link
            if availableLink in urlMap:
                continue

            req = Request(availableLink, headers={'User-Agent': 'Mozilla/5.0'})
            try:
                code = urllib.request.urlopen(req).getcode()
                urlMap[availableLink] = code
                file.write(f"{availableLink}\n")
            except URLError as e:
                if hasattr(e, 'code'):
                    urlMap[availableLink] = e.code
                else:
                    urlMap[availableLink] = "unknown exception"

            print(f"{availableLink} : {urlMap[availableLink]} ({index+1}/{len(links)})")
        file.close()


name = 'LinkChecker'
cmd = f'scrapy crawl {name}'
cmdline.execute(cmd.split())
